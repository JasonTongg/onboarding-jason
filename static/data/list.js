export const data = {
	ordered: [
		{
			tag: 'ol'
		},
		{
			tag: 'li',
			text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
			close: true,
			tab: 1
		},
		{
			tag: 'li',
			text: 'Incidunt magni consequatur adipisci officiis vero placeat consectetur',
			close: true,
			tab: 1
		},
		{
			tag: 'li',
			text: 'id facere,',
			tab: 1
		},
		{
			tag: 'ol',
			tab: 2
		},
		{
			tag: 'li',
			tab: 3,
			text: 'molestiae in,',
			close: true
		},
		{
			tag: 'li',
			tab: 3,
			text: 'ducimus necessitatibus hic ad esse consequuntur',
			close: true
		},
		{
			closeTag: 'ol',
			tab: 2
		},
		{
			closeTag: 'li',
			tab: 1
		},
		{
			tag: 'li',
			tab: 1,
			text: 'ducimus necessitatibus hic ad esse consequuntur',
			close: true
		},
		{
			closeTag: 'ol'
		}
	],
	unordered: [
		{
			tag: 'ul'
		},
		{
			tag: 'li',
			text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
			close: true,
			tab: 1
		},
		{
			tag: 'li',
			text: 'Incidunt magni consequatur adipisci officiis vero placeat consectetur',
			close: true,
			tab: 1
		},
		{
			tag: 'li',
			text: 'id facere,',
			tab: 1
		},
		{
			tag: 'ul',
			tab: 2
		},
		{
			tag: 'li',
			tab: 3,
			text: 'molestiae in,',
			close: true
		},
		{
			tag: 'li',
			tab: 3,
			text: 'ducimus necessitatibus hic ad esse consequuntur',
			close: true
		},
		{
			closeTag: 'ul',
			tab: 2
		},
		{
			closeTag: 'li',
			tab: 1
		},
		{
			tag: 'li',
			tab: 1,
			text: 'ducimus necessitatibus hic ad esse consequuntur',
			close: true
		},
		{
			closeTag: 'ul'
		}
	],
	inline: [
		{
			tag: 'ol',
			class: 'list-inline'
		},
		{
			tag: 'li',
			text: 'Lorem >',
			tab: 1
		},
		{
			tag: 'li',
			text: 'Ipsum >',
			tab: 1
		},
		{
			closeTag: 'ol'
		}
	],
	noStyle: [
		{
			tag: 'ul',
			class: 'list-nostyle'
		},
		{
			tag: 'li',
			text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
			close: true,
			tab: 1
		},
		{
			tag: 'li',
			text: 'Incidunt magni consequatur adipisci officiis vero placeat consectetur',
			close: true,
			tab: 1
		},
		{
			tag: 'li',
			text: 'id facere,',
			tab: 1
		},
		{
			tag: 'ul',
			tab: 2
		},
		{
			tag: 'li',
			tab: 3,
			text: 'molestiae in,',
			close: true
		},
		{
			tag: 'li',
			tab: 3,
			text: 'ducimus necessitatibus hic ad esse consequuntur',
			close: true
		},
		{
			closeTag: 'ul',
			tab: 2
		},
		{
			closeTag: 'li',
			tab: 1
		},
		{
			tag: 'li',
			tab: 1,
			text: 'ducimus necessitatibus hic ad esse consequuntur',
			close: true
		},
		{
			closeTag: 'ul'
		}
	]
}

export const data = {
	heading: [
		{
			tag: 'h1',
			text: 'Heading 1',
			close: true
		},
		{
			tag: 'h2',
			text: 'Heading 2',
			close: true
		},
		{
			tag: 'h3',
			text: 'Heading 3',
			close: true
		},
		{
			tag: 'h4',
			text: 'Heading 4',
			close: true
		},
		{
			tag: 'h5',
			text: 'Heading 5',
			close: true
		},
		{
			tag: 'h6',
			text: 'Heading 6',
			close: true
		}
	],
	headingStyle: [
		{
			tag: 'p',
			class: 'h1',
			text: 'Text size adjust h1',
			close: true
		},
		{
			tag: 'p',
			class: 'h2',
			text: 'Text size adjust h2',
			close: true
		},
		{
			tag: 'p',
			class: 'h3',
			text: 'Text size adjust h3',
			close: true
		},
		{
			tag: 'p',
			class: 'h4',
			text: 'Text size adjust h4',
			close: true
		},
		{
			tag: 'p',
			class: 'h5',
			text: 'Text size adjust h5',
			close: true
		},
		{
			tag: 'p',
			class: 'h6',
			text: 'Text size adjust h6',
			close: true
		}
	]
}

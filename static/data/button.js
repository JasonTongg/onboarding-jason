export const data = {
	buttonTemplate: [
		{
			tag: 'p'
		},
		{
			tag: 'button',
			class: 'btn--primary',
			text: 'Button Color',
			close: true,
			tab: 1
		},
		{
			tag: 'button',
			class: 'btn--ghost-primary',
			text: 'Button Color',
			close: true,
			tab: 1
		},
		{
			tag: 'button',
			class: 'btn--flat-primary',
			text: 'Button Color',
			close: true,
			tab: 1
		},
		{
			closeTag: 'p'
		}
	],
	iconTemplate: [
		{
			tag: 'p'
		},
		{
			tag: 'button',
			class: 'btn--primary btn-small btn-icon',
			tab: 1
		},
		{
			tag: 'i',
			class: 'fas fa-times fa-sm',
			tab: 2,
			close: true
		},
		{
			closeTag: 'button',
			tab: 1
		},
		{
			tag: 'button',
			class: 'btn--ghost-primary btn-small btn-icon',
			tab: 1
		},
		{
			tag: 'i',
			class: 'fas fa-times fa-sm',
			tab: 2,
			close: true
		},
		{
			closeTag: 'button',
			tab: 1
		},
		{
			tag: 'button',
			class: 'btn--flat-primary btn-small btn-icon',
			tab: 1
		},
		{
			tag: 'i',
			class: 'fas fa-times fa-sm',
			tab: 2,
			close: true
		},
		{
			closeTag: 'button',
			tab: 1
		},
		{
			closeTag: 'p'
		}
	],
	iconTextTemplate: [
		{
			tag: 'p'
		},
		{
			tag: 'button',
			class: 'btn--primary btn-small btn-icon',
			tab: 1,
			text: 'search'
		},
		{
			tag: 'i',
			class: 'fas fa-times fa-sm',
			tab: 2,
			close: true
		},
		{
			closeTag: 'button',
			tab: 1
		},
		{
			tag: 'button',
			class: 'btn--ghost-primary btn-small btn-icon',
			tab: 1,
			text: 'cancel'
		},
		{
			tag: 'i',
			class: 'fas fa-times fa-sm',
			tab: 2,
			close: true
		},
		{
			closeTag: 'button',
			tab: 1
		},
		{
			tag: 'button',
			class: 'btn--flat-primary btn-small btn-icon',
			tab: 1,
			text: 'profile'
		},
		{
			tag: 'i',
			class: 'fas fa-times fa-sm',
			tab: 2,
			close: true
		},
		{
			closeTag: 'button',
			tab: 1
		},
		{
			closeTag: 'p'
		}
	],
	button: [],
	size: [],
	icon: [],
	textIcon: []
}

const buttonList = [
	'primary',
	'secondary',
	'warning',
	'danger',
	'light-grey',
	'white'
]

buttonList.forEach(element => {
	const newButton = data.buttonTemplate.map(item => {
		if (item.tag === 'button') {
			if (item?.class?.includes('ghost')) {
				return { ...item, class: `btn--ghost-${element}` }
			} else if (item?.class?.includes('flat')) {
				return { ...item, class: `btn--flat-${element}` }
			}
			return { ...item, class: `btn--${element}` }
		} else {
			return item
		}
	})

	data.button = [...data.button, ...newButton]
})

const size = ['small', 'normal', 'large']

size.forEach(element => {
	const newButton = data.buttonTemplate.map(item => {
		if (item.tag === 'button') {
			if (element !== 'normal') {
				if (item?.class?.includes('ghost')) {
					return {
						...item,
						class: `btn--ghost-primary  btn--${element}`
					}
				} else if (item?.class?.includes('flat')) {
					return {
						...item,
						class: `btn--flat-primary  btn--${element}`
					}
				}
				return { ...item, class: `btn--primary  btn--${element}` }
			} else {
				if (item?.class?.includes('ghost')) {
					return {
						...item,
						class: `btn--ghost-primary `
					}
				} else if (item?.class?.includes('flat')) {
					return { ...item, class: `btn--flat-primary ` }
				}
				return { ...item, class: `btn--primary ` }
			}
		} else {
			return item
		}
	})

	data.size = [...data.size, ...newButton]

	const newButton2 = data.iconTemplate.map(item => {
		if (item.tag === 'button') {
			if (element !== 'normal') {
				if (item?.class?.includes('ghost')) {
					return {
						...item,
						class: `btn--ghost-primary btn--icon btn--${element}`
					}
				} else if (item?.class?.includes('flat')) {
					return {
						...item,
						class: `btn--flat-primary  btn--icon btn--${element}`
					}
				}
				return { ...item, class: `btn--primary btn--icon btn--${element}` }
			} else {
				if (item?.class?.includes('ghost')) {
					return {
						...item,
						class: `btn--ghost-primary btn--icon`
					}
				} else if (item?.class?.includes('flat')) {
					return { ...item, class: `btn--flat-primary btn--icon` }
				}
				return { ...item, class: `btn--primary btn--icon` }
			}
		} else {
			return item
		}
	})

	data.icon = [...data.icon, ...newButton2]

	const newButton3 = data.iconTextTemplate.map(item => {
		if (item.tag === 'button') {
			if (element !== 'normal') {
				if (item?.class?.includes('ghost')) {
					return {
						...item,
						class: `btn--ghost-primary btn--icon btn--${element}`
					}
				} else if (item?.class?.includes('flat')) {
					return {
						...item,
						class: `btn--flat-primary  btn--icon btn--${element}`
					}
				}
				return { ...item, class: `btn--primary btn--icon btn--${element}` }
			} else {
				if (item?.class?.includes('ghost')) {
					return {
						...item,
						class: `btn--ghost-primary btn--icon`
					}
				} else if (item?.class?.includes('flat')) {
					return { ...item, class: `btn--flat-primary btn--icon` }
				}
				return { ...item, class: `btn--primary btn--icon` }
			}
		} else {
			return item
		}
	})

	data.textIcon = [...data.textIcon, ...newButton3]
})

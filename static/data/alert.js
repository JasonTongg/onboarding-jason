export const data = {
	alert: [
		{
			tag: 'div',
			class: 'alert',
			text: 'Alert default'
		},
		{
			tag: 'div',
			class: 'alert--primary',
			text: 'Alert primary'
		},
		{
			tag: 'div',
			class: 'alert--secondary',
			text: 'Alert secondary'
		},
		{
			tag: 'div',
			class: 'alert--warning',
			text: 'Alert warning'
		},
		{
			tag: 'div',
			class: 'alert--danger',
			text: 'Alert danger'
		},
		{
			tag: 'div',
			class: 'alert--black',
			text: 'Alert black'
		},
		{
			tag: 'div',
			class: 'alert--light-grey',
			text: 'Alert light-grey'
		},
		{
			tag: 'div',
			class: 'alert--white',
			text: 'Alert white'
		}
	],
	alert2: [
		{
			tag: 'button',
			class: 'btn--primary js-trigger-alert',
			text: 'Alert 1',
			close: true
		},
		{
			tag: 'button',
			class: 'btn--primary js-trigger-alert',
			text: 'Alert 2',
			close: true
		},
		{
			tag: 'div',
			class: 'alert--popup'
		},
		{
			tag: 'div',
			class: 'alert--primary alert--popup-content',
			tab: 1
		},
		{
			tag: 'span',
			class: 'alert--close',
			tab: 2
		},
		{
			tag: 'i',
			class: 'fas fa-times',
			tab: 3,
			close: true
		},
		{
			closeTag: 'span',
			tab: 2
		},
		{
			tag: 'h4',
			text: 'Lorem Ipsum',
			tab: 2,
			close: true
		},
		{
			tag: 'p',
			text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
			tab: 2,
			close: true
		},
		{
			tag: 'hr',
			tab: 1
		},
		{
			tag: 'p',
			class: 'mb-0',
			text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam qui obcaecati tempora. Quod, nesciunt culpa sunt sit itaque voluptate molestiae in quia quos quam tempora sequi illo molestias? Rerum, sit?',
			tab: 1,
			close: true
		},
		{
			closeTag: 'div',
			tab: 1
		},
		{
			closeTag: 'div'
		}
	]
}

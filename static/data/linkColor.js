export const data = {
	color: [
		{
			tag: 'p',
			text: 'Link color = '
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: "'.link-primary'",
			class: 'link-primary'
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: '(Default link color)'
		},
		{
			closeTag: 'p'
		},
		{
			tag: 'p',
			text: 'Link color = '
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: "'.link-secondary'",
			class: 'link-secondary'
		},
		{
			closeTag: 'p'
		},
		{
			tag: 'p',
			text: 'Link color = '
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: "'.link-warning'",
			class: 'link-warning'
		},
		{
			closeTag: 'p'
		},
		{
			tag: 'p',
			text: 'Link color = '
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: "'.link-danger'",
			class: 'link-danger'
		},
		{
			closeTag: 'p'
		},
		{
			tag: 'p',
			text: 'Link color = '
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: "'.link-black'",
			class: 'link-black'
		},
		{
			closeTag: 'p'
		},
		{
			tag: 'p',
			text: 'Link color = '
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: "'.link-light-grey'",
			class: 'link-light-grey'
		},
		{
			closeTag: 'p'
		},
		{
			tag: 'p',
			text: 'Link color = '
		},
		{
			tag: 'a',
			tab: 1,
			close: true,
			text: "'.link-white'",
			class: 'link-white'
		},
		{
			closeTag: 'p'
		}
	]
}
